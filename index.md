---
title: About
date: "2019-03-03"
aliases: ["about-us","about","contact"]
---

The Task Force on Action and Perception is primarily concerned with the developmental processes involved in the emergence of representations of action and perception in humans and artificial agents in continual learning. These processes include the action-perception cycle, active perception, continual sensory-motor learning, environmental-driven scaffolding, and intrinsic motivation. As the algorithms for learning single tasks in controlled environments are improving, new challenges have gained relevance. They include multi-task learning, multimodal sensorimotor learning and lifelong adaptation to injury, growth and ageing. Members of this task force are strongly motivated by behavioural and neural data and develop mathematical and computational models to improve robot performance and/or attempt to unveil the underlying mechanisms that lead to continual adaptation to changing environments or embodiment and continual learning in open-ended environments. Members of this task force make extensive use of raw sensor data in multi-task robotic experiments.


<h2 class="pt-5 mb-3">Goal</h2>
The goals of this task force are:  

<ul>
  <li>to establish a network within CDS TC to develop algorithms capable of learning representations of action and perception in an open-ended unsupervised manner.</li>
  <li>to promote the use of computational models to understand better the underlying mechanisms of human and animal developmental cognition.</li>
  <li>to promote collaboration in developmental continual learning systems through dissemination events, including conferences, workshops and special issues for relevant journals.</li>
</ul>


<h2 class="pt-5 mb-3">Scope</h2>
<ul>
  <li>Emergence of representations via continual interaction</li>
  <li>Continual sensory-motor learning</li>
  <li>Active perception</li>
  <li>Environmental-driven scaffolding</li>
  <li>Intrinsic motivation</li>
</ul>

<h2 class="pt-5 mb-3">Membership</h2>
The Task Force (TF) consists of a Chair, a Vice-Chair and several Members. The Chair needs to be approved by the Cognitive and Developmental Systems Technical Committee. Task Force Chairs are appointed for two years with a possible extension for another two years.
If you are interested in joining this task force, please send an email to the current TF Chair with a link to your CV. Memberships end at the end of the calendar year and need to be renewed. 
