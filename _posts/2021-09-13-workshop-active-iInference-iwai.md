---
title: "International Workshop on Active Inference (IWAI)"
shorttitle: 

venue: 
date: 2021-09-13
dateend: 
location: Virtual – Bilbao
link: https://iwaiworkshop.github.io/
organizers: Christopher Buckley, Daniela Cialfi, Pablo Lanillos, Maxwell Ramstead, and Tim Verbelen

tf: [Action and Perception]
tags: [workshop]
submissions: 26
accepted: 10
posters: 6
speakers: 
participants: 50
---

