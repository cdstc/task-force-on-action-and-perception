---
title: "SI IEEE TCDS: Movement Sciences in Cognitive Systems"
shorttitle: 

journal: IEEE Transactions on Cognitive and Developmental Systems
date: 2023-01-06
location: 
link: https://cis.ieee.org/images/files/Documents/call-for-special-issues/Movement_Sciences_in_Cognitive_Systems_CFP_2022.pdf
organizers: Junpei Zhong, Ran Dong, Soichiro Ikuno, Yanan Li, Chenguang Yang

tf: [Action and Perception]
tags: [Special Issue]
submissions: 
accepted: 
speakers: 
participants: 
---

