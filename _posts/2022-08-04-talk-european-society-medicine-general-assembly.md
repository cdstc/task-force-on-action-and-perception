---
title: "Talk at the European Society of Medicine General Assembly"
shorttitle:

journal: 
date: 2022-08-04
dateend: 2022-08-06
location: NH Eurobuilding Hotel, Madrid, Spain
link: NH Eurobuilding Hotel, Madrid, Spain
organizers: European Society of Medicine

tf: [Action and Perception]
tags: [Conference, Talk]
submissions: 
accepted: 
speakers: 
participants: 
---

Cristiano Alessandro gave a plenary talk at the European Society of Medicine General Assembly, organised by European Society of Medicine.
