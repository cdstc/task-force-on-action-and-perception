---
title: "15th International Convention on Rehabilitation Engineering and Assistive Technology (i-CREATe 2022)"
shorttitle: 

date: 2022-08-26
dateend: 2022-08-28 
location: Hong Kong/Online
link: https://www6.rs.polyu.edu.hk/icreate/
venue: 
organizers: Kenneth Fong, Simon Wong, Sam Chan, Stella Cheng, Eddie Hai, Johnny Lam, Dino Lee, Hector Tsang, Alice Chan, Marko Chan, Junpei Zhong, etc
journal: 
publisher: 

tf: [Action and Perception]
tags: [conference]
submissions: 
accepted: 
speakers: 
participants: 
---



