---
title: "ICDL Workshop on Shared Affective Perception (WASP) – Virtual Workshop"
shorttitle: 

date: 2020-10-28
dateend: 
location: Valparaíso, Chile
link: https://www.whisperproject.eu/wasp2020
venue: IEEE Joint International Conference on Development and Learning and Epigenetic Robotics (ICDL-EpiRob 2020) 
organizers: Pablo Barros, and Alessandra Sciutti

journal: 
publisher: 

tf: [Action and Perception]
tags: [workshop]
submissions: 
accepted: 
speakers: 
participants: 
---

