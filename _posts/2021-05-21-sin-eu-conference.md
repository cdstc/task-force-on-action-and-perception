---
title: "Sino-EU Conference on Intelligent Robots and Automation"
shorttitle: 

date: 2021-05-21
dateend: 2021-05-22
location: Virtual/Shanghai, China
link: "http://www.gci-online.de/index.php?option=com_content&view=article&id=246:2021-sino-eu-conference-on-intelligent-robots-and-automation&catid=29:2016-02-18-16-02-30&Itemid=451"
venue: 
organizers: Liming Zeng, Rui Li, Nutan Chen, Mingchuan Zhou, Zhenshen Bing, You Zhou, Wei Wang, Jinjun Duan, Chenguang Yang, Fei Chen, Chao Liu, Junpei Zhong, Qiang Li

journal: 
publisher: 

tf: [Action and Perception]
tags: [conference]
submissions: 
accepted: 
speakers: 
participants: 
---



