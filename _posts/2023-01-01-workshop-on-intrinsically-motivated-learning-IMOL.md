---
title: "6th International Workshop on Intrinsically Motivated Open-ended Learning (IMOL)"
shorttitle: 

venue: 
date: 2023-01-01
dateend: 
location:  Sorbonne University, Paris, France
link: https://2022.imol-conf.org/
organizers: Stéphane Doncieux, Georg Martius, Emre Ugur and Sao Mai Nguyen

tf: [Action and Perception]
tags: [Summer School, workshop]
submissions: 
accepted: 
speakers: 
participants: 
---

