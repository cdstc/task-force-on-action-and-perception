---
title: "ICDL-EpiRob Workshop on Naturalistic Non-Verbal and Affective Human-Robot Interactions"
shorttitle: 

venue: "Joint IEEE International Conference on Development and Learning and Epigenetic Robotics (ICDL-EpiRob)"
journal: 
date: 2019-08-19
location: Oslo, Norway
link: https://nicolas-navarro-guerrero.gitlab.io/workshop-non-verbal-human-robot-interactions-icdl-epirob-2019/
organizers: Nicolás Navarro-Guerrero, Robert Lowe, and Chrystopher L. Nehaniv

tf: [Action and Perception]
tags: [Workshops]
submissions: 
accepted: 
speakers: 
participants: 
---

