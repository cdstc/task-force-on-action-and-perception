---
title: "5th Latin American Summer School in Computational Neuroscience (LACONEU)"
shorttitle: 

journal: 
date: 2023-01-09
dateend: 2023-01-20
location: Valparaiso, Chile
link: http://laconeu.cl/
organizers: María José Escobar, Patricio Orio, Wael El-Deredy, Podrigo Cofré


tf: [Action and Perception]
tags: [Summer School]
submissions: 80
accepted: 30
speakers: 
participants: 30-80
---

