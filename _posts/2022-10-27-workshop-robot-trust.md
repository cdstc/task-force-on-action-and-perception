---
title: "Workshop on Robot Trust for Symbiotic Societies"
shorttitle: 

venue: IROS 22
date: 2022-10-27
dateend: 
location: Kyoto, Japan
link: https://www.trustworthyrobots.eu/rtss-workshop/
organizers: Murat Kirtay, Erhan Oztop, Verena V. Hafner, Minoru Asada

tf: [Action and Perception]
tags: [workshop]
submissions: 
accepted: 
speakers: 
participants: 15-25
---

