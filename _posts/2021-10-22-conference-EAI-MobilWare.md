---
title: "10th International Conference on MOBILe Wireless MiddleWARE, Operating Systems, and Applications"
shorttitle: 

date: 2021-10-22
dateend: 2021-10-24 
location: Hohhot, China
link: https://mobilware.eai-conferences.org/2021/
venue: 
organizers: Fanyu Bu, Bo Gao, GongFa Li, Liang Zhao, Dalai Tang, Dalin Zhou, Junpei Zhong, Yinfeng Fang, Yuichiro Toda, Jinseok Woo, Xuezheng Yue, Haishan Bao, Wuyungerile Li, Zhong-Zhou Lan, Yana A, Suyalatu Dong, Song Yang, Baojun Sun, Tong Cui, Ting Wang, Linlin Xu, Peng Zhang, Xinwei Zhang, Yubo Guo, Hua Wu, Pingquan Wang, and Ming Jing Du.

journal: 
publisher: 

tf: [Action and Perception]
tags: [conference]
submissions: 
accepted: 
speakers: 
participants: 
---



