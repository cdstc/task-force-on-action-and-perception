---
title: "Talk at The EASE Fall School on Cognition Enabled Robot Manipulation"
shorttitle:

venue:
journal: 
date: 2022-09-19
dateend: 2022-09-23
location: Bremen, Germany
link: 
organizers: 

tf: [Action and Perception]
tags: [Symposium, Talk]
submissions: 
accepted: 
speakers: 
participants: 
---

Dr Alessandra Sciutti gave a lesson at the EASE Fall School on Cognition Enabled Robot Manipulation titled: “The role of interaction in cognitive robotics", 
