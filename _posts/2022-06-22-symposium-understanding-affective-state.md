---
title: "Symposium of Understanding the affective state of others: the role of vitality forms"
shorttitle: 

venue:  The Italian Association of Cognitive Science (AISC) midterm conference 2022
date: 2022-06-22
dateend: 2022-06-22
location:  Parma, Italy
link: https://www.aisc22midterm.it/#
organizers:  Giuseppe Di Cesare, Giada Lombardi, Liliana Ruta and Alessandra Sciutti

tf: [Action and Perception]
tags: [symposium]
submissions: 
accepted: 
speakers: 
participants:  about 20
---


