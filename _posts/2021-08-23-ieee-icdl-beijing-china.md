---
title: "10th IEEE International Conference on Development and Learning (ICDL)"
shorttitle: 

date: 2021-08-23
dateend: 2021-08-26 
location: Beijing, China
link: https://icdl-2021.org/
venue: 
organizers: Dingsheng Luo, Angelo Cangelosi, Alessandra Sciutti, Jing Chen, Zhengyou Zhang, Minoru Asada, Xihong Wu, Jochen Triesch, Tetsuya Ogata, Giulio Sandini, Yukie Nagai, Daniela Corbetta, Nicolás Navarro-Guerrero, Tianshu Qu, Weiwei Wan, and Yanlong Qin

journal: 
publisher: 

tf: [Action and Perception]
tags: [conference]
submissions: 
accepted: 
speakers: 
participants: 
---



