---
title: "SI Frontiers in Neurorobotics: Cognitive-Inspired Aspects of Robot Learning"
shorttitle: 

journal: Frontiers in Neurorobotics
date: 2023-06-01
location: 
link: 
organizers: Miguel Solis, Nicolás Navarro-Guerrero, and Francisco Cruz

tf: [Action and Perception]
tags: [Special Issue]
submissions: 
accepted: 
speakers: 
participants: 
---

