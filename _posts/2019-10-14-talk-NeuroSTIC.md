---
title: "Talk at journées NeuroSTIC"
shorttitle:

journal: 
date: 2019-10-14
dateend: 2019-10-15
location: Sophia-Antipolis, France
link: http://univ-cotedazur.fr/en/eur/ds4h/contents/news/neurostic-2019
organizers: Vincent Gripon, Martial Mermillod, Benoît Miramond

tf: [Action and Perception]
tags: [symposium]
submissions: 
accepted: 20
speakers: 
participants: 70
---

Sao Mai Nguyen gave a research talk on interactive and hierarchical reinforcement learning with intrinsic motivation/artificial curiosity at les journées NeuroSTIC in Sophia Antipolis.
