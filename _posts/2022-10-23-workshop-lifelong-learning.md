---
title: "Workshop on Lifelong Learning of High-level Cognitive and Reasoning Skills"
shorttitle: 

venue: IROS 22
date: 2022-10-23
dateend: 
location: Kyoto, Japan
link: https://lifelongrobotics.github.io/
organizers: Alper Ahmetoglu,Erhan Oztop, Tadahiro Taniguchi,Mete Tuluhan Akbulut, Justus Piater, Emre Ugur

tf: [Action and Perception]
tags: [workshop]
submissions: 
accepted: 
speakers: 
participants: 40-100
---

