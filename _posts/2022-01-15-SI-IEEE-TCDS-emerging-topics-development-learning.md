---
title: "2nd SI IEEE TCDS: Emerging Topics on Development and Learning"
shorttitle: 

journal: IEEE Transactions on Cognitive and Developmental Systems
date: 2022-06-01
location: 
link: https://cdstc.gitlab.io/icdl-2020/calls/special-issue/
organizers: Dingsheng Luo, Angelo Cangelosi, Alessandra Sciutti, Weiwei Wan, and Ana Tanevska

tf: [Action and Perception]
tags: [Special Issue]
submissions: 
accepted: 
speakers: 
participants: 
---

