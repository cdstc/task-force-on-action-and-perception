---
title: "SI Frontiers in Robotics and AI: Affective Shared Perception"
shorttitle: 

journal: Frontiers in Robotics and AI 
date: 2021-05-31
location: 
link: https://www.frontiersin.org/research-topics/16086/affective-shared-perception
organizers: Pablo Vinicius Alves De Barros, Alessandra Sciutti, Ginevra Castellano, Yukie Nagai

tf: [Action and Perception]
tags: [Special Issue]
submissions: 
accepted: 
speakers: 
participants: 
---

