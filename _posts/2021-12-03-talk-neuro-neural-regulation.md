---
title: "Talk at the International Conference on Neuroscience and Brain Disorders (Neuro-2021)"
shorttitle:

journal: 
date: 2021-12-03
dateend: 
location: Virtual 
link: 
organizers: 

tf: [Action and Perception]
tags: [Conference, Talk]
submissions: 
accepted: 
speakers: 
participants: 
---

Cristiano Alessandro gave a talk titled "Neural regulation of internal joint stresses and strains." 
