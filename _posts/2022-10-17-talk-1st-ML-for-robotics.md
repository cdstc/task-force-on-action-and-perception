---
title: "Talk at the 1st Machine Learning and Neuroscience for Robotics School"
shorttitle:

journal: 
date: 2022-10-17
dateend: 2022-10-20
location:  Moliets et Maa, France
link: https://sites.google.com/view/ecoledugt8/accueil
organizers:  Robotics Research Group, CNRS, France

tf: [Action and Perception]
tags: [talk]
submissions: 
accepted: 
speakers: 
participants: 30
---

Dr Céline Teulière was invited to give a plenary talk at the "1st Machine Learning and Neuroscience for Robotics" School, on the challenges of deep reinforcement learning in robotics. 
