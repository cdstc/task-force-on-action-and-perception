---
title: "IEEE RO-MAN Workshop: Cognitive Robotics for Interaction (CIRCE)"
shorttitle: 

date: 2020-08-31
dateend: 
location: Virtual
link: https://members.loria.fr/SIvaldi/roman-2020-workshop/
venue: "IEEE International Conference on Robot & Human Interactive Communication (IEEE RO-MAN)"
organizers: Alessandra Sciutti, Serena Ivaldi, Katrin Solveig Lohan, Giovanna Varni, and Alessia Vignolo

journal: 
publisher: 

tf: [Action and Perception]
tags: [workshop]
submissions: 
accepted: 
speakers: 
participants: 
---

