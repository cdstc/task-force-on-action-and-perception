---
title: "ICDL Workshop on Sensorimotor Interaction, Language and Embodiment of Symbols (SMILES) – Virtual Workshop"
shorttitle: 

journal: 
venue: "Joint IEEE International Conference on Development and Learning and Epigenetic Robotics (ICDL-EpiRob)"
date: 2020-10-26
dateend: 2020-10-27
location: Valparaíso, Chile
link: https://cdstc.gitlab.io/icdl-2020/program/workshops/#workshop1
organizers: Xavier Hinaut, Clément Moulin-Frier, Silvia Pagliarini, Chukiong Loo, Michael Spranger, Tadahiro Taniguchi, and Junpei Zhong

tf: [Action and Perception]
tags: [Workshop]
submissions: 
accepted: 6
speakers: 
participants: 
---

