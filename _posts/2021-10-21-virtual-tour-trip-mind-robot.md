---
title: "iCub – viaggio nella mente di un robot"
shorttitle: "Virtual Tour – Trip in the Mind of a Robot (Italian)"

journal: 
date: 2021-10-21
dateend: 
location: 
link: https://adventr.io/published/rbcs-contact-fds-2021-32876876
organizers: Linda Lastrico, Alessandra Sciutti

tf: [Action and Perception]
tags: [Virtual Tour, Video]
submissions: 
accepted: 
speakers: 
participants: 
---

Organization of the Virtual Tour – "Trip in the Mind of a Robot" for the Festival Della Scienza (Genova). The tour consists of a video adventure (in Italian) providing insights into robots' current capabilities, including perception and action.
