---
title: "NER Minisymposium on Multiscale Brain Modelling"
shorttitle: 

date: 2021-05-05
dateend: 
location: Online
link: https://embs.papercept.net/conferences/conferences/NER21/program/NER21_ContentListWeb_2.html#wea2
venue: 10th International IEEE EMBS Conference on Neural Engineering
organizers: Claudia Casellato, Alice Geminiani, Cristiano Alessandro, and Egidio D'Angelo

journal: 
publisher: 

tf: [Action and Perception]
tags: [symposium, workshop]
submissions: 
accepted: 
speakers: 
participants: 
---

