---
title: "IEEE SMC Special Session: Human-in-the-loop Machine Learning and Its Applications"
shorttitle: 

venue: "IEEE International Conference on Systems, Man, and Cybernetics (IEEE SMC)"
date: 2020-10-11
dateend: 2020-10-14
location: Toronto, Canada
link: https://jonizhong.weebly.com/smc2020.html
organizers: Joni Zhong, Mark Elshaw, Yanan Li, Stefan Wermter, and Xiaofeng Liu

tf: [Action and Perception]
tags: [Special Session]
submissions: 5
accepted: 
speakers: 
participants: 
---

