---
title: "IEEE SMC Workshop in Human-in-the-loop Learning System and its User-centric Methods"
shorttitle: 

date: 2021-10-17
dateend: 2021-10-28
location: Melbourne, Australia
link: http://ieeesmc2021.org/special-sessions/
venue: IEEE International Conference on Systems, Man, and Cybernetics (SMC)
organizers: Junpei Zhong, Ahmad Lotfi, David Ada Adama, Wei Hong Chin

journal: 
publisher: 

tf: [Action and Perception]
tags: [workshop]
submissions: 
accepted: 
speakers: 
participants: 
---

