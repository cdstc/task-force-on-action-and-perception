---
title: "SI: Real-Time Machine Vision Acceleration Technology and Applications"
shorttitle: 

journal: Journal of Real-Time Image Processing
date: 2023-04-23
location: 
link: https://link.springer.com/collections/efjhhjghai
organizers: Joni Zhong, Qiong Chang, Tinghui Ouyang, Weimin Wang, Daniel Hernandez Juarez

tf: [Action and Perception]
tags: [Special Issue]
submissions: 
accepted: 
speakers: 
participants: 
---

