---
title: "Invited Talk at the The NeuroEngineering School"
shorttitle:

journal: 
date: 2022-07-18
dateend: 2022-07-22
location: Genova, Italy
link: https://www.neuroengineering.eu/
organizers: European Society of Medicine

tf: [Action and Perception]
tags: [Conference, Talk]
submissions: 
accepted: 
speakers: 
participants: 
---

Prof. Alessandra Sciutti was invited a plenary talk at the "NeuroEngineering School", titled "The Sensorimotor Bases of Interaction with the World and with Others"
