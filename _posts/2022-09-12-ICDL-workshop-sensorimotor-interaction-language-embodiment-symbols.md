---
title: "3rd ICDL Workshop on Sensorimotor Interaction, Language and Embodiment of Symbols (SMILES)"
shorttitle: 

journal: 
venue: "IEEE International Conference on Development (ICDL)"
date: 2022-09-12
dateend: 2022-09-13
location: Queen Mary University, London/Online
link: https://sites.google.com/view/smiles-workshop/
organizers: Xavier Hinaut, Clément Moulin-Frier, Silvia Pagliarini, Michael Spranger, Tadahiro Taniguchi, Anne Warlaumont, and Joni Zhong

tf: [Action and Perception]
tags: [Workshop]
submissions: 
accepted: 
speakers: 5
participants: 
---

