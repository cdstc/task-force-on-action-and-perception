---
title: "2nd ICDL Workshop on Sensorimotor Interaction, Language and Embodiment of Symbols (SMILES) – Virtual Workshop"
shorttitle: 

journal: 
venue: "IEEE International Conference on Development (ICDL)"
date: 2021-08-31
dateend: 
location: Online
link: https://sites.google.com/view/smiles-workshop/
organizers: Xavier Hinaut, Clément Moulin-Frier, Silvia Pagliarini, Michael Spranger, Tadahiro Taniguchi, Anne Warlaumont, and Joni Zhong

tf: [Action and Perception]
tags: [Workshop]
submissions: 
accepted: 
speakers: 
participants: 115
---

