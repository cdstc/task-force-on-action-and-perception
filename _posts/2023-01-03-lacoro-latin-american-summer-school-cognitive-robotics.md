---
title: "2nd Latin American Summer School on Cognitive Robotics (LACORO)"
shorttitle: 

journal: 
date: 2023-01-03
dateend: 2023-01-06
location: Santiago, Chile
link: https://www.lacoro.org/
organizers: Nicolás Navarro-Guerrero, Miguel Solis, Francisco Cruz

tf: [Action and Perception]
tags: [Summer School]
submissions: 
accepted: 
speakers: 
participants: 
---

