---
title: "4th ICDL Workshop on Sensorimotor Interaction, Language and Embodiment of Symbols (SMILES)"
shorttitle: 

journal: 
venue: "IEEE International Conference on Development (ICDL)"
date: 2023-11-09
dateend: 2023-11-13
location: Macau, China
link: https://sites.google.com/view/smiles-workshop/
organizers: Xavier Hinaut, Clément Moulin-Frier, Silvia Pagliarini, Michael Spranger, Tadahiro Taniguchi, Anne Warlaumont, and Joni Zhong

tf: [Action and Perception]
tags: [Workshop]
submissions: 
accepted: 
speakers: 
participants: 
---

