---
title: "5th International Workshop on Intrinsically Motivated Open-ended Learning (IMOL)"
shorttitle: 

venue: 
date: 2022-04-01
dateend: 
location: Max Planck Institute for Intelligent Systems, Tübingen, Germany.
link: https://2022.imol-conf.org/
organizers: Georg Martius, Vieri Giuliano Santucci, Rania Rayyes, Gianluca Baldassarre

tf: [Action and Perception]
tags: [Summer School, workshop]
submissions: 26 
accepted: 15, (12 as posters and 3 as contributed talk)
speakers: 
participants: 50
---

