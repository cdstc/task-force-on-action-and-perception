---
title: "Symposium: From humans to robots: the role of vitality forms in social interactions"
shorttitle: 

venue: The Annual Conference of the Italian Society of Psychophysiology and Cognitive Neuroscience
date: 2022-09-16
dateend: 2022-09-16
location: Udine, Italy
link: 
organizers:  Giuseppe Di Cesare, Giada Lombardi, Liliana Ruta and Alessandra Sciutti

tf: [Action and Perception]
tags: [symposium]
submissions: 
accepted: 
speakers: 
participants:  about 50
---


