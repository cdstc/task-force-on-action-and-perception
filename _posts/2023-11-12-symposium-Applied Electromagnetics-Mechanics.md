---
title: "21st International Symposium on Applied Electromagnetics and Mechanics"
shorttitle: 

venue: 
date: 2023-11-12
dateend: 2023-11-15
location: Tokyo, Japan
link: 
organizers:  Ran Dong, et al.

tf: [Action and Perception]
tags: [symposium]
submissions: 400
accepted: 450
speakers: 
participants:  500
---


