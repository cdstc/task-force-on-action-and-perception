---
title: "ICDL-EpiRob Workshop on Continual Unsupervised Sensorimotor Learning"
shorttitle: 

venue: "Joint IEEE International Conference on Development and Learning and Epigenetic Robotics (ICDL-EpiRob)"
date: 2018-09-17
location: Tokyo, Japan
link: https://conferences.au.dk/icdl-epirob-2018-workshop/
organizers: Nicolás Navarro-Guerrero, Sao Mai Nguyen, Erhan Öztop and Junpei Zhong

tf: [Action and Perception]
tags: [workshop]
submissions: 
accepted: 
speakers: 
participants: 
---

