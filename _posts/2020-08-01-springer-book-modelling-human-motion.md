---
title: "Book: Modelling Human Motion – From Human Perception to Robot Design"
shorttitle: 

date: 2020-08-01
dateend: 
location: 
link: 
venue: 
organizers: Nicoletta Noceti, Alessandra Sciutti, and Francesco Rea

journal: 
publisher: Springer Nature Switzerland AG

tf: [Action and Perception]
tags: [book]
submissions: 
accepted: 
speakers: 
participants: 
---

