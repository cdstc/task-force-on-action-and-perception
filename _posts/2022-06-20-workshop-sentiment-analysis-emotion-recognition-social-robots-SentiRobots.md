---
title: "1st International Workshop on Sentiment Analysis and Emotion Recognition for Social Robots (SentiRobots)"
shorttitle: 

venue: "18th International Conference on Intelligent Environments (IE)"
date: 2022-06-20
dateend: 2022-06-23
location:  Biarritz, France
link: https://sentirobots.ucsp.edu.pe/2022/
organizers: 

tf: [Action and Perception]
tags: [workshop]
submissions: 
accepted: 
speakers: 
participants: 
---

Nicolás Navarro-Guerrero will participate in the program/technical committee of the 1st International Workshop on Sentiment Analysis and Emotion Recognition for Social Robots (SentiRobots) to be held in Biarritz, France, between the 20th and 23rd of June 2022. Co-located with the 18th International Conference on Intelligent Environments (IE).

