---
title: "3rd Workshop on Adapted intEraction with SociAl Robots  (UcAESAW)"
shorttitle: 

venue: "30th ACM Conference on User Modeling, Adaptation and Personalization"
date: 2022-07-04
dateend: 2022-07-04
location:  Barcelona, Spain
link: https://caesar2022.di.unito.it/
organizers: Berardina (Nadja) De Carolis, Cristina Gena, Antonio Lieto, Silvia Rossi, Alessandra Sciutti

tf: [Action and Perception]
tags: [workshop]
submissions: 
accepted: 
speakers: 
participants: 
---


