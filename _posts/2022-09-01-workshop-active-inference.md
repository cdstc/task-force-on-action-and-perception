---
title: "3rd International Workshop on Active Inference"
shorttitle: 

venue: 
date: 2022-09-30
dateend: 
location: Radboud Universiteit Nijmegen, The Netherlands
link: https://iwaiworkshop.github.io/
organizers: Pablo Lanillos, Christopher Buckley, Daniela Cialfi, Maxwell Ramstead, Noor Sajid, Hideaki Shimazaki, Tim Verbelen

tf: [Action and Perception]
tags: [workshop]
submissions: 28
accepted: 8 presentations, 18 posters
speakers: 
participants: 50
---

