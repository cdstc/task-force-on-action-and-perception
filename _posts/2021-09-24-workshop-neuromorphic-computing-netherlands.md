---
title: "Workshop on Neuromorphic Computing Netherlands (NCN2021)"
shorttitle: 

venue: 
date: 2021-09-24
dateend: 
location: Amsterdam (hybrid)
link: https://www.cwi.nl/research/groups/machine-learning/events/workshop-on-neuromorphic-computing
organizers: Sander Bothe, Federico Corradi, and Pablo Lanillos

tf: [Action and Perception]
tags: [workshop]
submissions: 10
accepted: 8
speakers: 
participants: 30
onsite: 20
online: 10
---

