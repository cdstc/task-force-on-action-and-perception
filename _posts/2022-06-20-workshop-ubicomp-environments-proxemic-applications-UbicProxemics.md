---
title: "1st International Workshop on Ubicomp Environments and Proxemic Applications (UbicProxemics)"
shorttitle: 

venue: "18th International Conference on Intelligent Environments (IE)"
date: 2022-06-20
dateend: 2022-06-23
location:  Biarritz, France
link: https://ubicproxemics.ucsp.edu.pe/2022/
organizers: 

tf: [Action and Perception]
tags: [workshop]
submissions: 
accepted: 
speakers: 
participants: 
---

Nicolás Navarro-Guerrero will participate in the program/technical committee of the 1st International Workshop on Ubicomp Environments and Proxemic Applications (UbicProxemics) to be held in Biarritz, France, between the 20th and 22nd of June 2022. Co-located with the 18th International Conference on Intelligent Environments (IE).

