---
title: "REAL 2021 – Robot open-Ended Autonomous Learning competition"
shorttitle: 

date: 2021-08-23
dateend: 2021-12-23
location: 
link: https://icdl-2021.org/competition/
venue: IEEE International Conference on Development and Learning (ICDL 2021) 
organizers: Emilio Cartoni, Davide Montella, Vieri Giuliano Santucci, Gianluca Baldassarre

journal: 
publisher: 

tf: [Action and Perception]
tags: [competition]
submissions: 
accepted: 
speakers: 
participants: 
---

