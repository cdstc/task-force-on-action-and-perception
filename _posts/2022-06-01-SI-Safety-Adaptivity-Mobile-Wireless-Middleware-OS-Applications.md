---
title: "SI Conference Issue: Safety and Adaptivity in Mobile Wireless Middleware, Operating Systems, and Applications"
shorttitle: 

journal: Hindawi
date: 2022-06-01
location: 
link: https://www.hindawi.com/journals/scn/si/547983/
organizers: Joni Zhong, Alessandro Di Nuovo, Dalin Zhou, Fanyu Bu

tf: [Action and Perception]
tags: [Special Issue]
submissions: 5
accepted: 2
speakers: 
participants: 
---

