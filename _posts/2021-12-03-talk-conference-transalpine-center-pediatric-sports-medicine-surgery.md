---
title: "Talk at the 3rd Conference of the Transalpine Center of Pediatric Sports Medicine and Surgery"
shorttitle:

journal: 
date: 2021-12-03
dateend: 
location: San Gerardo Hospital, Monza (MB), Italy
link: 
organizers: Transalpine Center of Pediatric Sports Medicine and Surgery, University of Milano-Bicocca

tf: [Action and Perception]
tags: [Conference, Talk]
submissions: 
accepted: 
speakers: 
participants: 
---

Cristiano Alessandro gave a plenary talk titled "Cruciate Ligament Injuries in Young Athletes" at the 3rd Conference of the Transalpine Center of Pediatric Sports Medicine and Surgery, organised by Transalpine Center of Pediatric Sports Medicine and Surgery, University of Milano-Bicocca.
