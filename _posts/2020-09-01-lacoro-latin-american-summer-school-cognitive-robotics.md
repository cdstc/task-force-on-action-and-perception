---
title: "1st Latin American Summer School on Cognitive Robotics (LACORO)"
shorttitle: 

journal: 
date: 2020-09-01
dateend: 2020-09-04
location: Valparaíso, Chile
link: https://www.lacoro.org/
organizers: Nicolás Navarro-Guerrero, Miguel Solis, Sao Mai Nguyen

tf: [Action and Perception]
tags: [Summer School]
submissions: 
accepted: 
speakers: 
participants: 
---

