---
title: "IUI Workshop on Adapted intEraction with SociAl Robots (cAESAR)"
shorttitle: 

venue: "Annual Meeting of the Intelligent User Interfaces Community (IUI)"
date: 2020-03-17
dateend: 2020-03-20
location: Cagliari, Italy
link: https://caesar2020.di.unito.it/index.html
organizers: B. De Carolis, C. Gena, A. Lieto, S. Rossi & A. Sciutti

tf: [Action and Perception]
tags: [workshop]
submissions: 
accepted: 
speakers: 
participants: 
---

